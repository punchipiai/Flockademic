import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

import { ProfileList } from '../../src/components/profileList/component';

const mockProps = {
  profiles: [
    {
      name: 'Arbitrary name',
      sameAs: 'https://orcids.org/arbitrary-orcid',
    },
  ],
};

it('should not render anything when there are no profiles', () => {
  const mockProfiles = [];

  const overview = shallow(<ProfileList {...mockProps} profiles={mockProfiles} />);

  expect(overview).toBeEmptyRender();
});

it('should display profile names', () => {
  const overview = shallow(<ProfileList {...mockProps} />);

  expect(toJson(overview)).toMatchSnapshot();
});
