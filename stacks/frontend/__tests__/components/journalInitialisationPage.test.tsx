jest.mock('../../src/services/periodical', () => ({
  initialiseJournal: jest.fn().mockReturnValue(Promise.resolve({ id: 'arbitrary journal id' })),
}));

import { JournalInitialisationPage } from '../../src/components/journalInitialisationPage/component';
import { Spinner } from '../../src/components/spinner/component';

import { mount, shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

const routerProps = {
  params: {},
};

it('should display a spinner while initialising the journal', () => {
  const page = shallow(<JournalInitialisationPage match={routerProps}/>);

  expect(page.find(Spinner)).toExist();
});

it('should display an error message when something went wrong initialising the Journal', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.initialiseJournal.mockReturnValueOnce(Promise.reject('Initialisation error'));

  const page = shallow(<JournalInitialisationPage match={routerProps}/>);

  setImmediate(() => {
    page.update();
    expect(page.find('.callout.alert')).toExist();
    done();
  });
});

it('should redirect to the management page after initialising a new Journal', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.initialiseJournal.mockReturnValueOnce(Promise.resolve({
    result: { identifier: 'some_uuid' },
  }));

  const page = shallow(<JournalInitialisationPage match={routerProps}/>);

  setImmediate(() => {
    page.update();
    expect(page.find('Redirect[to="/journal/some_uuid/manage"]')).toExist();
    done();
  });
});
