jest.mock('../../src/services/publishScholarlyArticle', () => ({
  publishScholarlyArticle: jest.fn().mockReturnValue(Promise.resolve({
    datePublished: 'arbitrary date',
    identifier: 'arbitrary_identifier',
  })),
}));
jest.mock('../../src/services/scholarlyArticle', () => ({
  fetchScholarlyArticle: jest.fn().mockReturnValue(Promise.resolve({
    author: {
      identifier: 'Arbitrary account id',
    },
    description: 'Arbitrary abstract',
    identifier: 'arbitrary_identifier',
    name: 'Arbitrary name',
  })),
}));
jest.mock('../../../../lib/utils/periodicals', () => ({
  isArticleAuthor: jest.fn().mockReturnValue(true),
}));

import { publishScholarlyArticle } from '../../src/resources/publishScholarlyArticle';

const mockContext = {
  body: {
    targetCollection: { identifier: 'arbitrary article id' },
  },
  database: {} as any,
  session: { identifier: 'Arbitrary ID', account: { identifier: 'Arbitrary account ID' } },

  headers: {},
  method: 'POST' as 'POST',
  params: [ '/articles/arbitrary_article_id/publish', 'arbitrary_article_id' ],
  path: '/articles/arbitrary_article_id/publish',
  query: null,
};

it('should error when no parameters were specified', () => {
  const promise = publishScholarlyArticle({ ...mockContext, body: undefined });

  return expect(promise).rejects.toEqual(new Error('Please specify an article to publish.'));
});

it('should error when no article to publish was specified', () => {
  const invalidRequest: any = {
    ...mockContext.body,
    targetCollection: undefined,
  };
  const promise = publishScholarlyArticle({ ...mockContext, body: invalidRequest });

  return expect(promise).rejects.toEqual(new Error('Please specify an article to publish.'));
});

it('should error when the user does not have a session', () => {
  const promise = publishScholarlyArticle({
    ...mockContext,
    session: new Error('No session found'),
  });

  return expect(promise).rejects.toEqual(new Error('You do not appear to be logged in.'));
});

it('should error when the user does not have an account', () => {
  const promise = publishScholarlyArticle({
    ...mockContext,
    session: { identifier: 'Arbitrary session ID' },
  });

  return expect(promise)
    .rejects.toEqual(new Error('You can only publish an article if you have created an account.'));
});

it('should error when the given article could not be retrieved', () => {
  const mockedArticleService = require.requireMock('../../src/services/scholarlyArticle');
  mockedArticleService.fetchScholarlyArticle.mockReturnValueOnce(Promise.reject(new Error('Some error')));

  const promise = publishScholarlyArticle(mockContext);

  return expect(promise).rejects.toEqual(new Error('Some error'));
});

it('should error when the given article could not be found', () => {
  const mockedArticleService = require.requireMock('../../src/services/scholarlyArticle');
  mockedArticleService.fetchScholarlyArticle.mockReturnValueOnce(Promise.resolve(null));

  const promise = publishScholarlyArticle({
    ...mockContext,
    body: {
      ...mockContext.body,
      targetCollection: {
        ...mockContext.body.targetCollection,
        identifier: 'some_id',
      },
    },
  });

  return expect(promise).rejects.toEqual(new Error('Could not find an article with ID some_id.'));
});

it('should error when the given article is not managed by the current user', () => {
  const mockedArticleService = require.requireMock('../../src/services/scholarlyArticle');
  mockedArticleService.fetchScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    creator: undefined,
    identifier: 'arbitrary_slug',
  }));

  const promise = publishScholarlyArticle(mockContext);

  return expect(promise).rejects.toEqual(new Error('You can only publish your own articles.'));
});

it('should error when the given article is authored by someone other than the current user', () => {
  const mockedIsArticleAuthor = require.requireMock('../../../../lib/utils/periodicals').isArticleAuthor;
  mockedIsArticleAuthor.mockReturnValueOnce(false);

  const promise = publishScholarlyArticle(mockContext);

  return expect(promise).rejects.toEqual(new Error('You can only publish your own articles.'));
});

it('should error when the given article does not yet have a valid abstract', () => {
  const mockedArticleService = require.requireMock('../../src/services/scholarlyArticle');
  mockedArticleService.fetchScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    author: { identifier: 'Arbitrary author' },
    identifier: 'arbitrary_id',
    name: 'Arbitrary abstract',
  }));

  const promise = publishScholarlyArticle(mockContext);

  return expect(promise)
    .rejects.toEqual(new Error('The article has to have a valid abstract before it can be published.'));
});

it('should error when the given article does not yet have a valid name', () => {
  const mockedArticleService = require.requireMock('../../src/services/scholarlyArticle');
  mockedArticleService.fetchScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    author: { identifier: 'Arbitrary author' },
    description: 'Arbitrary abstract',
    identifier: 'arbitrary_id',
  }));

  const promise = publishScholarlyArticle(mockContext);

  return expect(promise)
    .rejects.toEqual(new Error('The article has to have a valid name before it can be published.'));
});

it('should return the publication date of a newly published article', () => {
  const mockedPublishScholarlyArticleService = require.requireMock('../../src/services/publishScholarlyArticle');
  mockedPublishScholarlyArticleService.publishScholarlyArticle.mockReturnValueOnce(Promise.resolve({
    datePublished: 'Some date',
    identifier: 'Some article',
  }));

  const promise = publishScholarlyArticle({
    ...mockContext,
    body: {
      ...mockContext.body,
      targetCollection: { identifier: 'Some article' },
    },
    session: {
      account: { identifier: 'Some account ID' },
      identifier: 'Arbitrary session ID',
    },
  });

  return expect(promise).resolves.toEqual({
    result: {
      author: { identifier: 'Some account ID' },
      datePublished: 'Some date',
      identifier: 'Some article',
    },
    targetCollection: { identifier: 'Some article' },
  });
});

it('should error when the article could not be published, and log it', (done) => {
  const mockedPublishScholarlyArticleService = require.requireMock('../../src/services/publishScholarlyArticle');
  mockedPublishScholarlyArticleService.publishScholarlyArticle
    .mockReturnValueOnce(Promise.reject(new Error('Some error')));
  console.log = jest.fn();

  const promise = publishScholarlyArticle(mockContext);

  expect(promise)
    .rejects.toEqual(new Error('There was a problem publishing the article, please try again.'));

  setImmediate(() => {
    expect(console.log.mock.calls.length).toBe(1);
    expect(console.log.mock.calls[0][0]).toBe('Database error:');
    expect(console.log.mock.calls[0][1]).toEqual(new Error('Some error'));

    done();
  });
});
